# NDD Docker Caches

A bunch of cache servers to speed up build process.

A [HTTPd](https://httpd.apache.org/docs/current/fr/programs/httpd.html) server acts as a proxy for:

- the DEB files cache server [APT Cacher NG](https://www.unix-ag.uni-kl.de/~bloch/acng/)
- the GEM files cache server [GemStash](https://github.com/bundler/gemstash)



## Installation

Clone the repository:

```shell
git clone git@bitbucket.org:ndd-docker/ndd-docker-caches.git
cd ndd-docker-caches
```



## Usage

NB: the script `bin/caches.sh` provides some helper utilities (`./bin/caches.sh --help`).

### Initialisation

Build the images:

```shell
docker-compose build
```

Volumes should be created in order to persist cached data which is the whole point after all:

```shell
docker volume create --name ddidier-deb-cache-data
docker volume create --name ddidier-gem-cache-data
```

### From the host

Then start all containers with:

```shell
docker compose up
```

Once running:

- the HTTPd proxy is available at `http://localhost:12080`
- The APT Cacher NG console is available `http://localhost:12081/acng-report.html`

### From a building image

To use these caches from a building a Docker image, you need to connect the proxy to the default `bridge` network:

```shell
docker network connect bridge ddidier-http-cache
```

Then find the IP of the HTTPd proxy in this network:

```shell
export DOCKER_HTTP_PROXY="http://$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' ddidier-http-cache)"
```

Then pass it as a build argument, for example:

```shell
docker build \
    --build-arg http_proxy=$DOCKER_HTTP_PROXY \
    --build-arg HTTP_PROXY=$DOCKER_HTTP_PROXY \
    -t my/image .
```



## Enabling caches

### DEB files cache

There is multiple ways to [use an APT proxy](https://help.ubuntu.com/community/AptGet/Howto#Setting_up_apt-get_to_use_a_http-proxy). In a building image, exporting `http_proxy` as a build argument is enough:

```shell
docker build \
    --build-arg http_proxy=$DOCKER_HTTP_PROXY \
    --build-arg HTTP_PROXY=$DOCKER_HTTP_PROXY \
    -t my/image .
```

### GEM files cache

With the `gem` application, it seems that you must replace the official HTTPS source, **which is insecure**, before using the proxy as a parameter:

```shell
gem sources --remove https://rubygems.org/
gem sources --add    http://rubygems.org/
gem install --http-proxy http://$DOCKER_HTTP_PROXY bundler
```

With the `bundler` application, exporting `http_proxy` as a build argument is enough:

```shell
http_proxy=http://$DOCKER_HTTP_PROXY bundle install
```

