#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

function caches() {
    action="$1"

    case $action in
        init)    _caches_init;;
        size)    _caches_size;;
        start)   _caches_start;;
        stop)    _caches_stop;;
        *)       _caches_help;;
    esac
}

function _caches_init() {
    pushd $PROJECT_DIR && docker-compose build ; popd
    docker volume create --name ddidier-deb-cache-data
    docker volume create --name ddidier-gem-cache-data
}

function _caches_size() {
    local deb_size=$(sudo du -hs /var/lib/docker/volumes/ddidier-deb-cache-data | cut -f 1)
    local gem_size=$(sudo du -hs /var/lib/docker/volumes/ddidier-gem-cache-data | cut -f 1)
    echo "DEB cache size = $deb_size"
    echo "GEM cache size = $gem_size"
}

function _caches_start() {
    (
        sleep 3
        echo "Connecting container 'ddidier-http-cache' to default bridge network..."
        docker network connect bridge ddidier-http-cache
        echo "Container connected"
    )&

    pushd $PROJECT_DIR && docker-compose up ; popd
}

function _caches_stop() {
    pushd $PROJECT_DIR && docker-compose down ; popd
}

function _caches_help() {
    echo "Usage: ./caches.sh init|size|start|stop"
    echo "  init .... build the images and create the persistent volumes"
    echo "  size .... display the persistent volumes size"
    echo "  start ... start all the containers and connect the proxy to the default bridge network"
    echo "  stop .... stop all the containers"
}

caches $@
